var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];
//конструктор студент
function Student(name,point){
	this.name = name;
	this.point = point
}; 

Student.prototype.show = function() {
	console.log('Студент %s набрал %d баллов', this.name, this.point);
}
//конструктор список студентов
function StudentList(name,studentsAndPoints){
	this.name = name;  
	if (studentsAndPoints!==undefined) {
		for (var i=0, imax = studentsAndPoints.length;i < imax; i+=2){
			this.add(studentsAndPoints[i],studentsAndPoints[i+1]);
		};
	}
};

StudentList.prototype = Object.create(Array.prototype);
StudentList.prototype.constructor = StudentList;
StudentList.prototype.add = function(name,point){
 this.push(new Student(name,point));
};
//создаем список студентов
var hj2 = new StudentList('HJ-2',studentsAndPoints);
//добавляем студентоа
hj2.add('Иван Иванов',70);
hj2.add('Петр Петров',20);
//создаем новую группу 
var html7 = new StudentList('HTML-7');

//добавим студентов
html7.add('Мария Смирнова',10);
html7.add('Егор Кошкин',50);

//добавляем метод show для списка
StudentList.prototype.show = function(){
	console.log('Группа %s (%d  студентов):',this.name,this.length);
	this.forEach(function(item){
		item.show();
	});
};
//выводим список
hj2.show();
html7.show();

//переводим первого студента из группы HJ-2 в HTML-7
hj2.splice(0,1).forEach(function(item){
	html7.push(item);
}
);

//переопределяем метод valueOf
Student.prototype.valueOf = function() { 
	return this.point; 
};

//добавляем метод Max для списка студентов
StudentList.prototype.max = function(){
	var max = Math.max.apply(null,this);
	return this.find(function(item){
		     return item.point ==  max; 
	       }); 
}

console.log("Лучший в групппе");
//определяем лучшего студента в группе
var best = html7.max();
best.show();